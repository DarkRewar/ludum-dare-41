﻿using HomeSafe.Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HomeSafe
{
    public class GameManager : MonoBehaviour
    {
        public Character[] Characters;

        public GameManager CharacterPrefab;

        private int _day = 0;
        public int Day { get { return _day; } }

        #region SINGLETON
        public static GameManager Current;

        private void Awake()
        {
            Current = this;
        }
        #endregion

        // Use this for initialization
        void Start()
        {
            BeginDay();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void BeginDay()
        {
            ++_day;

        }

        public void EndDay()
        {
        }

        public void TalkWith(Character character)
        {

        }
    }
}
