﻿using HomeSafe.Characters;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HomeSafe.UI
{
    public class History : MonoBehaviour {

        public static History Current;

        public ScrollRect ScrollRect;
        public Text EntriesDisplay;

        private List<string> _entries;

        private int _currentDay
        {
            get { return GameManager.Current.Day; }
        }

        private void Awake()
        {
            Current = this;
        }

        // Use this for initialization
        void Start () {
            _entries = new List<string>();
	    }
	
	    // Update is called once per frame
	    void LateUpdate () {
            if(ScrollRect != null)
            {
                ScrollRect.content.sizeDelta = new Vector2(0, EntriesDisplay.preferredHeight);
            }
	    }

        public void AddEntry(Character character, string text)
        {
            string entry = "[Day " + _currentDay + "] " + character.FullName + " : " + text;
            _entries.Add(entry);

            if(EntriesDisplay != null)
            {
                EntriesDisplay.text += entry + "\n";
            }
        }

        public void AddEntry(string character, string text)
        {
            string entry = "[Day " + _currentDay + "] " + character + " : " + text;
            _entries.Add(entry);

            if (EntriesDisplay != null)
            {
                EntriesDisplay.text += entry + "\n";
            }
        }
    }
}
