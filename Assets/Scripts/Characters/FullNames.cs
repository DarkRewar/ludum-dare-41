﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace HomeSafe.Characters
{
    public static class FullNames
    {
        public static string[] MaleNames = new string[]
        {
            "Henri",
            "George",
            "Peter",
            "Curtis",
            "Hervé",
            "Adrien",
            "Emmanuel",
            "Roger",
            "Gueudric",
            "Benoit",
            "Julien",
            "Benjamin",
            "Alexandre",
            "Thierry",
            "Jean",
            "Jean-Eude",
            "Quentin",
            "Sébastien",
            "Christopher",
            "Dorian",
            "Vincent",
            "Raphaël",
            "Léonard",
            "Gaëtan",
            "Fabien",
            "Nicolas",
            "Cédric",
            "Tristan",
            "Kévin",
            "Thomas",
            "Maxime",
            "Bastien",
            "Dylan",
            "Daniel",
            "Florian"
        };

        public static string[] FemaleNames = new string[]
        {
            "Tiffanny",
            "Guylaine",
            "Clémence",
            "Cléa",
            "Alex",
            "Camille",
            "Charlotte",
            "Julie",
            "Léa",
            "Juliette",
            "Flora",
            "Florence",
            "Jeanine",
            "Valérie",
            "Anna",
            "Elodie",
            "Hermione",
            "Celestine",
            "Marine",
            "Anne",
            "Lisa",
            "Coralie",
            "Elise",
            "Carmen",
            "Marion",
            "Alcia",
            "Anaïs",
            "Chloé",
            "Elsa",
            "Jade",
            "Melina",
            "Audrey",
        };

        public static string GetRandomName(Gender gender)
        {
            if(gender == Gender.Male)
            {
                return MaleNames[Random.Range(0, MaleNames.Length)];
            }
            else
            {
                return FemaleNames[Random.Range(0, FemaleNames.Length)];
            }
        }
    }
}
