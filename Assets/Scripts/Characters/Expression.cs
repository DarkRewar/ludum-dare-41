﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace HomeSafe.Characters
{
    [CreateAssetMenu(fileName = "New Expression", menuName = "HomeSafe/Expression")]
    public class Expression : ScriptableObject
    {
        public Emotion Emotion;

        [Header("Male")]
        public Sprite MaleMouth;

        public Sprite MaleLeftEye;
        public Sprite MaleRightEye;

        public Sprite MaleLeftEyeMask;
        public Sprite MaleRightEyeMask;

        public Sprite MaleLeftEyebrow;
        public Sprite MaleRightEyebrow;

        [Header("Female")]
        public Sprite FemaleMouth;

        public Sprite FemaleLeftEye;
        public Sprite FemaleRightEye;

        public Sprite FemaleLeftEyeMask;
        public Sprite FemaleRightEyeMask;

        public Sprite FemaleLeftEyebrow;
        public Sprite FemaleRightEyebrow;
    }
}
