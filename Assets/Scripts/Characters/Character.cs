﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HomeSafe.Characters
{
    public enum Gender
    {
        Male = 0,
        Female = 1
    }

    public class Character : MonoBehaviour
    {
        public string FullName;
        public uint Age;
        public Gender Gender;

        public Stats Stats;

        public Color EyesColor;
        public Color SkinColor;
        public Color HairColor;

        [Header("Head")]
        public SpriteRenderer Hair;
        public SpriteRenderer Head;
        public SpriteRenderer Nose;

        public Sprite[] MaleHairs;
        public Sprite[] FemaleHairs;
        public Sprite[] MaleHeads;
        public Sprite[] FemaleHeads;

        public Sprite MaleNose;
        public Sprite FemaleNose;

        public Face Face;

        [Header("Body")]
        public SpriteRenderer Clothes;
        public SpriteRenderer Body;

        public Sprite[] MaleClothes;
        public Sprite[] MaleBodies;
        public Sprite[] FemaleClothes;
        public Sprite[] FemaleBodies;

        public SpriteRenderer PupilLeft;
        public SpriteRenderer PupilRight;

        private Stats _baseStats;

        // Use this for initialization
        void Start()
        {
            Stats.Generate();
            _baseStats = Stats.Clone();
            Face.Start();
            Generate();

            StartCoroutine(PupilMovement());
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.R))
            {
                Generate();
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                SetEmotion((Emotion)Random.Range(0, 9));
            }
        }

        void Generate()
        {
            Gender = (Gender)Random.Range(0, 2);
            FullName = FullNames.GetRandomName(Gender);
            Age = (uint)Random.Range(16, 50);
            EyesColor = Colors.GetRandomEyes();
            SkinColor = Colors.GetRandomSkins();
            HairColor = Colors.GetRandomHairs();

            Face.Gender = Gender;
            SetEmotion(Emotion.Neutral);

            if (Gender == Gender.Male)
            {
                Hair.sprite = MaleHairs[Random.Range(0, MaleHairs.Length)];
                Head.sprite = MaleHeads[Random.Range(0, MaleHeads.Length)];
                Nose.sprite = MaleNose;

                int body = Random.Range(0, MaleClothes.Length);
                Clothes.sprite = MaleClothes[body];
                Body.sprite = MaleBodies[body];
            }
            else
            {
                Hair.sprite = FemaleHairs[Random.Range(0, FemaleHairs.Length)];
                Head.sprite = FemaleHeads[Random.Range(0, FemaleHeads.Length)];
                Nose.sprite = FemaleNose;

                int body = Random.Range(0, FemaleClothes.Length);
                Clothes.sprite = FemaleClothes[body];
                Body.sprite = FemaleBodies[body];
            }

            Hair.color = HairColor;
            Head.color = SkinColor;
            Nose.color = SkinColor;
            PupilLeft.color = EyesColor;
            PupilRight.color = EyesColor;

            Body.color = SkinColor;
        }

        public void SetEmotion(Emotion emotion)
        {
            Face.SetEmotion(emotion);
        }

        IEnumerator PupilMovement()
        {
            Vector3 basePos = Vector3.zero;
            float time = 0;

            while (true)
            {
                time = 0;
                while (time < 2f)
                {
                    PupilLeft.transform.localPosition = Vector3.Lerp(new Vector3(-0.163f, 0, 0), new Vector3(0.163f, 0, 0), time / 2f);
                    PupilRight.transform.localPosition = Vector3.Lerp(new Vector3(-0.163f, 0, 0), new Vector3(0.163f, 0, 0), time / 2f);
                    time += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }


                yield return new WaitForSeconds(2f);


                time = 0;
                while (time < 2f)
                {
                    PupilLeft.transform.localPosition = Vector3.Lerp(new Vector3(0.163f, 0, 0), new Vector3(-0.163f, 0, 0), time / 2f);
                    PupilRight.transform.localPosition = Vector3.Lerp(new Vector3(0.163f, 0, 0), new Vector3(-0.163f, 0, 0), time / 2f);
                    time += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }

                yield return new WaitForSeconds(2f);
            }
        }
    }
}
