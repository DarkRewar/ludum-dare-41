﻿using UnityEngine;

namespace HomeSafe.Characters
{
    [System.Serializable]
    public class Stats
    {
        [Header("Fixed Stats")]

        /// <summary>
        /// Intelligence du personnage.
        /// </summary>
        [Range(0, 100)]
        public uint Intelligence;

        /// <summary>
        /// Si le personnage est susceptible.
        /// </summary>
        [Range(0, 100)]
        public uint Susceptibilite;

        /// <summary>
        /// De 0 (Pessimiste) à 100 (Optimiste)
        /// </summary>
        [Range(0, 100)]
        public uint Sens;

        [Header("In Game Stats")]

        /// <summary>
        /// Le moral du personnage.
        /// </summary
        [Range(0, 100)]
        public uint Moral;

        /// <summary>
        /// Hypocrisie envers le joueur.
        /// </summary>
        [Range(0, 100)]
        public uint Hypocrisie;

        /// <summary>
        /// La faim
        /// </summary>
        [Range(0, 100)]
        public uint Faim;

        /// <summary>
        /// Fatigue du personnage.
        /// </summary>
        [Range(0, 100)]
        public uint Fatigue;

        public Stats Clone()
        {
            return new Stats()
            {
                Intelligence = Intelligence,
                Susceptibilite = Susceptibilite,
                Sens = Sens,
                Moral = Moral,
                Hypocrisie = Hypocrisie,
                Faim = Faim,
                Fatigue = Fatigue,
            };
        }

        public void Generate()
        {
            Intelligence = (uint)Random.Range(0, 100);
            Susceptibilite = (uint)Random.Range(0, 100);
            Sens = (uint)Random.Range(0, 100);

            Moral = (uint)Random.Range(35, 65);
            Hypocrisie = (uint)Random.Range(35, 65);
            Faim = (uint)Random.Range(35, 65);
            Fatigue = (uint)Random.Range(35, 65);
        }
    }
}
