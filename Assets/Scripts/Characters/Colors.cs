﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HomeSafe.Characters
{
    public static class Colors
    {
        public static Color[] Eyes = new Color[] {
            new Color((float)0x51 / 255, (float)0x40 / 255, (float)0x38 / 255),
            new Color((float)0x9D / 255, (float)0x72 / 255, (float)0x5D / 255),
            new Color((float)0x9D / 255, (float)0x88 / 255, (float)0x7D / 255),
            new Color((float)0x4D / 255, (float)0x9D / 255, (float)0x97 / 255),
            new Color((float)0x38 / 255, (float)0x51 / 255, (float)0x4F / 255),
        };

        public static Color[] Skins = new Color[] {
            new Color((float)0xEE / 255, (float)0xC9 / 255, (float)0xB7 / 255),
            new Color((float)0xD6 / 255, (float)0xBA / 255, (float)0x8E / 255),
            new Color((float)0xDB / 255, (float)0x99 / 255, (float)0x79 / 255),
            new Color((float)0x9D / 255, (float)0x5B / 255, (float)0x43 / 255),
            new Color((float)0x44 / 255, (float)0x12 / 255, (float)0x12 / 255),
        };

        public static Color[] Lips = new Color[] {
            new Color((float)0x42 / 255, (float)0x19 / 255, (float)0x1D / 255),
            new Color((float)0xFC / 255, (float)0x5E / 255, (float)0x6E / 255),
            new Color((float)0xF5 / 255, (float)0x99 / 255, (float)0x9C / 255),
            new Color((float)0xBE / 255, (float)0x91 / 255, (float)0x7C / 255),
            new Color((float)0xF4 / 255, (float)0xC3 / 255, (float)0xCD / 255),
        };

        public static Color[] Hairs = new Color[] {
            new Color((float)0x1F / 255, (float)0x1F / 255, (float)0x21 / 255),
            new Color((float)0x54 / 255, (float)0x32 / 255, (float)0x21 / 255),
            new Color((float)0xEF / 255, (float)0xE0 / 255, (float)0x70 / 255),
            new Color((float)0xF9 / 255, (float)0x87 / 255, (float)0x5A / 255),
            new Color((float)0xED / 255, (float)0xED / 255, (float)0xED / 255),
        };

        public static Color GetRandomEyes()
        {
            return Eyes[Random.Range(0, Eyes.Length)];
        }

        public static Color GetRandomSkins()
        {
            return Skins[Random.Range(0, Skins.Length)];
        }

        public static Color GetRandomLips()
        {
            return Lips[Random.Range(0, Lips.Length)];
        }

        public static Color GetRandomHairs()
        {
            return Hairs[Random.Range(0, Hairs.Length)];
        }
    }
}
