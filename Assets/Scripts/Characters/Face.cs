﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace HomeSafe.Characters
{
    public enum Emotion
    {
        Neutral,
        Joy,
        Wrath,
        Fear,
        Sad,
        Smile,
        Exhaust,
        Surprise,
        Sly,
    }

    [System.Serializable]
    public class Face
    {
        public Emotion Emotion = Emotion.Neutral;

        public SpriteRenderer Mouth;

        public SpriteRenderer LeftEye;
        public SpriteRenderer RightEye;

        public SpriteRenderer LeftEyebrow;
        public SpriteRenderer RightEyebrow;

        public Expression CurrentExpression {
            get { if (Expressions.Length <= (int)Emotion) return null; return Expressions[(int)Emotion]; }
        }

        [Header("Expressions")]
        public Expression[] Expressions;

        public Gender Gender { get; set; }

        private SpriteMask _leftEyeMask;
        private SpriteMask _rightEyeMask;

        public void Start()
        {
            _leftEyeMask = LeftEye.GetComponent<SpriteMask>();
            _rightEyeMask = RightEye.GetComponent<SpriteMask>();
        }

        public void SetEmotion(Emotion emotion)
        {
            Emotion = emotion;

            Expression expression = CurrentExpression;

            if(expression != null)
            {
                Mouth.sprite = Gender == Gender.Male ? expression.MaleMouth : expression.FemaleMouth;

                LeftEye.sprite = Gender == Gender.Male ? expression.MaleLeftEye : expression.FemaleLeftEye;
                RightEye.sprite = Gender == Gender.Male ? expression.MaleRightEye : expression.FemaleRightEye;

                _leftEyeMask.sprite = Gender == Gender.Male ? expression.MaleLeftEyeMask : expression.FemaleLeftEyeMask;
                _rightEyeMask.sprite = Gender == Gender.Male ? expression.MaleRightEyeMask : expression.FemaleRightEyeMask;

                //LeftEyebrow.sprite = Gender == Gender.Male ? expression.MaleLeftEyebrow : expression.FemaleLeftEyebrow;
                //RightEyebrow.sprite = Gender == Gender.Male ? expression.MaleRightEyebrow : expression.FemaleRightEyebrow;
            }
            else
            {
                Debug.LogError(string.Format("L'expression pour l'emotion {0} n'existe pas.", emotion.ToString()));
            }
        }
    }
}
