﻿using Cradle;
using HomeSafe.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace HomeSafe.Dialogs
{
    public class CharactersMacros : RuntimeMacros
    {
        private DialogManager _dialogManager;

        /// <summary>
        /// Change l'emotion du personnage.
        /// </summary>
        /// <param name="emotion"></param>
        [RuntimeMacro]
        public void changeEmotion(string emotion)
        {
            if(_dialogManager == null)
            {
                _dialogManager = DialogManager.Current;
            }

            for (int i = 0; i < 9; ++i)
            {
                if (emotion.ToLower() == ((Emotion)i).ToString().ToLower())
                {
                    _dialogManager.Character.SetEmotion((Emotion)i);
                    return;
                }
            }
        }
    }
}
