﻿using Cradle;
using HomeSafe.Characters;
using HomeSafe.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HomeSafe.Dialogs
{
    public class DialogManager : MonoBehaviour
    {
        public static DialogManager Current;

        public Story Story;

        public Character Character;

        public CanvasGroup CharacterDialog; 
        public Text CharacterText;

        public CanvasGroup ChoicesDialog;
        public Button[] ChoicesBtns;

        private void Awake()
        {
            Current = this;
        }

        // Use this for initialization
        void Start () {
            StartCoroutine(Begin());

            CharacterDialog.GetComponent<Button>().onClick.AddListener(() => {
                string[] tags = Story.CurrentPassage.Tags;
                for(int i = 0; i < tags.Length; ++i)
                {
                    if(tags[i] == "close")
                    {
                        OnExit();
                        return;
                    }
                }
                DisplayChoices(true);
            });
	    }

        IEnumerator Begin()
        {
            yield return new WaitForSeconds(1f);

            StartStory();
        }

        private void SetStoryVar(string key, StoryVar val)
        {
            if (Story.Vars.ContainsKey(key))
            {
                Story.Vars[key] = val;
            }
        }

        public void StartStory()
        {
            DisplayChoices(false);

            SetStoryVar("fullname", Character.FullName);
            SetStoryVar("moral", (int)Character.Stats.Moral);
            SetStoryVar("hypocrisie", (int)Character.Stats.Hypocrisie);
            SetStoryVar("faim", (int)Character.Stats.Faim);
            SetStoryVar("fatigue", (int)Character.Stats.Fatigue);

            Story.OnPassageEnter += PassageEnter;
            Story.OnOutput += PassageOutput;
            Story.OnPassageDone += PassageDone;
            Story.OnPassageExit += PassageExit;

            Story.Begin();
        }

        void OnExit()
        {
            Story.OnPassageEnter -= PassageEnter;
            Story.OnOutput -= PassageOutput;
            Story.OnPassageDone -= PassageDone;
            Story.OnPassageExit -= PassageExit;

            Character.Stats.Moral = (uint)Story.Vars["moral"];
            Character.Stats.Hypocrisie = (uint)Story.Vars["hypocrisie"];
            Character.Stats.Faim = (uint)Story.Vars["faim"];
            Character.Stats.Fatigue = (uint)Story.Vars["fatigue"];

            CharacterDialog.alpha = 0;
        }

        private void PassageEnter(StoryPassage obj)
        {
            CharacterText.text = "";
            DisplayChoices(false);
        }

        private void PassageDone(StoryPassage obj)
        {
            string dialog = "";
            foreach(StoryText text in Story.GetCurrentText())
            {
                dialog += text.Text;
                Debug.Log(text);
            }

            StartCoroutine(DisplayText(dialog));
            History.Current.AddEntry(Character, dialog);

            int i = 0;
            foreach(StoryLink link in Story.GetCurrentLinks())
            {
                Debug.Log(link.Text.Replace("$fullname", Character.FullName));
                if (i < 3)
                {
                    string choiceText = link.Text.Replace("$fullname", Character.FullName);
                    ChoicesBtns[i].gameObject.SetActive(true);
                    ChoicesBtns[i].GetComponentInChildren<Text>().text = choiceText;
                    ChoicesBtns[i].onClick.AddListener(() => {
                        History.Current.AddEntry("Me", choiceText);
                        Story.GoTo(link.PassageName);
                    });
                    ++i;
                }
            }
        }

        private void PassageExit(StoryPassage obj)
        {
            Debug.Log("PassageExit");
        }

        private void PassageOutput(StoryOutput obj)
        {
            //CharacterText.text += obj.Text;
        }

        private void DisplayChoices(bool display = true)
        {
            CharacterDialog.alpha = display ? 0 : 1;
            CharacterDialog.interactable = !display;
            CharacterDialog.blocksRaycasts = !display;

            ChoicesDialog.alpha = display ? 1 : 0;
            ChoicesDialog.interactable = display;
            ChoicesDialog.blocksRaycasts = display;

            if (!display)
            {
                for (int i = 0; i < 3; ++i)
                {
                    ChoicesBtns[i].onClick.RemoveAllListeners();
                    ChoicesBtns[i].gameObject.SetActive(false);
                }
            }
        }

        private IEnumerator DisplayText(string text)
        {
            CharacterText.text = "";
            foreach (char c in text)
            {
                CharacterText.text += c;
                yield return new WaitForSeconds(0.025f);
            }
        }
    }
}